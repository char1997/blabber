import uuid
import pymongo
from flask import Flask, jsonify, request
app = Flask(__name__)


mongoClient = pymongo.MongoClient("mongodb://mongo:27017")
mongoCollection = mongoClient["cs2304"]["blabs"]

@app.route("/blabs", methods = ['GET'])
def get_all_blabs():
    createdSince = request.args.get('createdSince', default = 0, type = int)
    # blabsSince = filter(lambda blab : blab.postTime >= createdSince, blabs)
    for blab in mongoCollection.find():
        if blab.postTime >= createdSince:
            b = blab.copy()
            b["id"] = str(item["_id"])
            del b["_id"]
    return make_response(jsonify(blabsSince), 200)


@app.route('/blabs', methods = ['POST'])
def add_blab():
    content = request.json
    #response = jsonify(id=uuid.uuid1(), postTime=gmtime(), author=content.author, message=content.message)
    response = mongoCollection.insert_one({postTime=gmtime(), author=content.author, message=content.message})
    
    return make_reponse(jsonify({postTime=gmtime(), author=content.author, message=content.message, id=response.inserted_id}), 201)

    
@app.route('/blabs/<blab>', methods = ['DELETE'])
def delete_blab(blab):
    length = len(blabs)
    result = mongoCollection.delete_one({"_id":blab.id}) 
    if (result.acknowledged):
        return make_response(jsonify({}), 200)
    return make_response(jsonify({}), 404)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
    

